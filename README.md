# Requirements

* you have installed JDK 11


# Local development 

* open terminal
* ```
    ./mvnw clean package 
    ./mvnw spring-boot:run
  ```
  
* open web browser http://localhost:8080/swagger-ui/index.html#/

# Overall design

* there 3 main packages category, product, core
* note that currently each domain (category and product) is in own package but in fact this could be refactored into maven modules to achieve better separation.

## Core

* contains infrastructure service - e.g. validation, exception handling.. this doesn't have any reference to any other domain package

## Category

* domain package of aggregate root Category
* contains api package, which can be references from any other modules

## Product

* domain package of aggregate root Product - it contains references to category
* uses category.api package


# Testing

* BDD sample tests - covers 100% controllers, 100% services code and 100% repository code
