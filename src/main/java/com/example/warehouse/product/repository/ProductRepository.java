package com.example.warehouse.product.repository;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.warehouse.category.api.CategoryConstants;
import com.example.warehouse.product.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("SELECT p.id, p.name, p.description, p.price"
            + " FROM product p"
            + " WHERE exists(SELECT 1 from product_category pc WHERE pc.category_id = :category_id AND pc.product = p.id) ")
    List<Product> findAllByProductCategory(@Param("category_id") Long categoryId);

    @Query("SELECT p.id, p.name, p.description, p.price"
            + " FROM product p"
            + " WHERE"
            + "     exists(SELECT 1 from product_category pc WHERE pc.category_id = :category_id AND pc.product = p.id) OR "
            + "     exists(SELECT 1 from category c"
            + "         JOIN category sub_c ON sub_c.path LIKE (CONCAT(c.path, '" + CategoryConstants.CATEGORY_PATH_SEPARATOR + "%'))"
            + "         JOIN product_category pc on sub_c.id = pc.category_id AND pc.product = p.id "
            + "         WHERE c.id = :category_id) ")
    List<Product> findAllByProductCategoriesAndSubCategories(@Param("category_id") Long categoryId);

}
