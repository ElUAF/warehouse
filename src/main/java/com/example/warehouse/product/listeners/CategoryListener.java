package com.example.warehouse.product.listeners;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.example.warehouse.category.api.events.CategoryPreDeletedEvent;
import com.example.warehouse.product.service.ProductService;

@Component
class CategoryListener {

    private final ProductService productService;

    private CategoryListener(ProductService productService) {
        this.productService = productService;
    }

    /**
     * When category should be deleted we need to clean up all references in products in order to be sure that DB integrity is not compromised by this
     * action.
     *
     * @param categoryPreDeletedEvent
     *         event containing category ref, required to identify which products are affected
     */
    @EventListener
    public void categoryDeleted(CategoryPreDeletedEvent categoryPreDeletedEvent) {
        productService.removeAllProductsFromCategory(categoryPreDeletedEvent.getCategoryRef());
    }
}
