package com.example.warehouse.product.rest;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.warehouse.product.dto.CreateProductRequest;
import com.example.warehouse.product.entity.Product;
import com.example.warehouse.product.service.ProductService;

@RestController
@RequestMapping("/api/")
public class ProductController {

    private static final String PRODUCT_PATH = "products";

    private final ProductService productService;

    ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(PRODUCT_PATH + "/{id}")
    @NotNull
    public Product findProductById(@PathVariable("id") long id) {
        return productService.findProductById(id);
    }

    @PostMapping(PRODUCT_PATH)
    @NotNull
    public Product createProduct(@RequestBody CreateProductRequest createProductRequest) {
        return productService.createProduct(createProductRequest);
    }

    @PostMapping(PRODUCT_PATH + "{product_id}/category/{category_id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void addProductToCategory(@PathVariable("product_id") long productId,
                                     @PathVariable("category_id") long categoryId) {
        productService.addProductToCategory(productId, categoryId);
    }

    @DeleteMapping(PRODUCT_PATH + "{product_id}/category/{category_id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void removeProductFromCategory(@PathVariable("product_id") long productId,
                                          @PathVariable("category_id") long categoryId) {
        productService.removeProductFromCategory(productId, categoryId);
    }

    @GetMapping("categories/{category_id}/" + PRODUCT_PATH)
    public List<Product> findProductsByCategoryId(@PathVariable("category_id") long categoryId) {
        return productService.findProductsByCategoryIdRecursively(categoryId);
    }
}
