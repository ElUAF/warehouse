package com.example.warehouse.product.entity;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;

import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.product.validation.ProductDescription;
import com.example.warehouse.product.validation.ProductName;
import com.example.warehouse.product.validation.ProductPrice;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Product {

    @Getter
    @Id
    private Long id;

    @Getter
    @ProductName
    private String name;

    @Getter
    @ProductDescription
    private String description;

    @Getter
    @ProductPrice
    private int price;

    @Getter
    @JsonIgnore
    private List<ProductCategory> productCategories;

    @Builder(toBuilder = true)
    public Product(Long id, String name, String description, int price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.productCategories = List.of();
    }

    public Product addCategory(@NotNull CategoryRef category) {
        if (isInCategory(category)) {
            return this;
        } else {
            var copiedProduct = toBuilder().build();
            copiedProduct.setProductCategories(
                    Stream.concat(
                            productCategories.stream(),
                            Stream.of(ProductCategory.builder().categoryId(category.getId()).build())
                    ).collect(Collectors.toList()));
            return copiedProduct;
        }
    }

    public Product removeCategory(@NotNull CategoryRef category) {
        if (!isInCategory(category)) {
            return this;
        } else {
            var copiedProduct = toBuilder().build();
            copiedProduct.setProductCategories(
                    productCategories.stream()
                            .filter(productCategory -> !Objects.equals(productCategory.getCategoryId(), category.getId()))
                            .collect(Collectors.toList()));
            return copiedProduct;
        }
    }

    private boolean isInCategory(@NotNull CategoryRef category) {
        return productCategories.stream().anyMatch(productCategory -> Objects.equals(productCategory.getCategoryId(), category.getId()));
    }

    protected void setProductCategories(List<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

    public boolean hasCategory(@NotNull CategoryRef category) {
        return productCategories.stream().anyMatch(productCategory -> Objects.equals(productCategory.getCategoryId(), category.getId()));
    }
}
