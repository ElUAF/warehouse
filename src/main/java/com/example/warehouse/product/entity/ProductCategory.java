package com.example.warehouse.product.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ProductCategory {

    @Getter
    private Long categoryId;

    @Builder
    ProductCategory(Long categoryId) {
        this.categoryId = categoryId;
    }
}
