package com.example.warehouse.product.dto;

import com.example.warehouse.product.validation.ProductDescription;
import com.example.warehouse.product.validation.ProductName;
import com.example.warehouse.product.validation.ProductPrice;
import com.fasterxml.jackson.annotation.JsonCreator;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class CreateProductRequest {

    @Getter
    @ProductName
    private String name;

    @Getter
    @ProductDescription
    private String description;

    @Getter
    @ProductPrice
    private Integer price;

    @Builder
    @JsonCreator
    public CreateProductRequest(String name, String description, int price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }
}
