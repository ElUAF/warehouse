package com.example.warehouse.product.api.exception;

import com.example.warehouse.core.exception.AbstractEntityNotFoundException;
import com.example.warehouse.product.entity.Product;

public class ProductNotFoundException extends AbstractEntityNotFoundException {

    public ProductNotFoundException(final Long id) {
        super(Product.class, id);
    }
}
