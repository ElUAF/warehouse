package com.example.warehouse.product.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.category.api.exception.CategoryNotFoundException;
import com.example.warehouse.category.api.service.CategoryPublicService;
import com.example.warehouse.product.api.exception.ProductNotFoundException;
import com.example.warehouse.product.dto.CreateProductRequest;
import com.example.warehouse.product.entity.Product;

public interface ProductService {

    @NotNull
    Product findProductById(long id);

    @NotNull
    Product createProduct(@NotNull CreateProductRequest request);

    /**
     * Adds existing product to existing category.
     *
     * @throws ProductNotFoundException
     *         if product is not found by given product ID
     * @throws CategoryNotFoundException
     *         if category is not found by given category ID
     */
    void addProductToCategory(long productId,
                              long categoryId);

    /**
     * Removes existing product from existing category
     *
     * @throws ProductNotFoundException
     *         if product is not found by given product ID
     * @throws CategoryNotFoundException
     *         if category is not found by given category ID
     */
    void removeProductFromCategory(long productId,
                                   long categoryId);

    /**
     * Removes all products from given category. If there isn't any product in given category, nothing happens.
     *
     * @param category
     *         the category reference - should be valid reference loaded from DB
     * @see CategoryPublicService
     */
    void removeAllProductsFromCategory(CategoryRef category);

    /**
     * Finds all products, which are in given category or any super category of that given category.
     *
     * @return products for given category
     * @throws CategoryNotFoundException
     *         if given category doesn't exists
     */
    @NotNull
    List<Product> findProductsByCategoryIdRecursively(long categoryId);
}
