package com.example.warehouse.product.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.category.api.service.CategoryPublicService;
import com.example.warehouse.core.validation.ValidationService;
import com.example.warehouse.product.api.exception.ProductNotFoundException;
import com.example.warehouse.product.dto.CreateProductRequest;
import com.example.warehouse.product.entity.Product;
import com.example.warehouse.product.repository.ProductRepository;

@Service
@Transactional(readOnly = true)
class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ValidationService validationService;
    private final CategoryPublicService categoryPublicService;

    ProductServiceImpl(ProductRepository productRepository, ValidationService validationService, CategoryPublicService categoryPublicService) {
        this.productRepository = productRepository;
        this.validationService = validationService;
        this.categoryPublicService = categoryPublicService;
    }

    @NotNull
    @Override
    public Product findProductById(long id) {
        return findExistingProduct(id);
    }

    @Transactional
    @NotNull
    @Override
    public Product createProduct(@NotNull CreateProductRequest request) {
        validationService.validate(request);

        var product = Product.builder()
                .name(request.getName())
                .description(request.getDescription())
                .price(request.getPrice())
                .build();

        return productRepository.save(product);
    }

    @Transactional
    @Override
    public void addProductToCategory(long productId,
                                     long categoryId) {
        var product = findExistingProduct(productId);
        var category = categoryPublicService.findExistingCategory(categoryId);

        var updatedProduct = product.addCategory(category);

        productRepository.save(updatedProduct);
    }

    @Transactional
    @Override
    public void removeProductFromCategory(long productId,
                                          long categoryId) {
        var product = findExistingProduct(productId);
        var category = categoryPublicService.findExistingCategory(categoryId);

        removeProductFromCategoryInternal(product, category);
    }

    @Override
    public void removeAllProductsFromCategory(final CategoryRef category) {
        productRepository.findAllByProductCategory(category.getId()).forEach(
                product -> removeProductFromCategoryInternal(product, category));
    }

    @NotNull
    @Override
    public List<Product> findProductsByCategoryIdRecursively(long categoryId) {
        var category = categoryPublicService.findExistingCategory(categoryId);

        return productRepository.findAllByProductCategoriesAndSubCategories(categoryId);
    }

    private void removeProductFromCategoryInternal(@NotNull Product product, @NotNull CategoryRef category) {
        var updatedProduct = product.removeCategory(category);
        productRepository.save(updatedProduct);
    }

    private Product findExistingProduct(final long productId) {
        var product = productRepository.findById(productId);

        if (product.isEmpty()) {
            throw new ProductNotFoundException(productId);
        }

        return product.get();
    }
}
