package com.example.warehouse.core.events;

public abstract class AbstractEntityPreDeletedEvent extends AbstractWarehouseEvent {

    private final Class entityClass;
    private final Long id;

    public AbstractEntityPreDeletedEvent(final Class entityClass, final Long id) {
        this.entityClass = entityClass;
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
