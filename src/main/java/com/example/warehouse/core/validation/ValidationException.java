package com.example.warehouse.core.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;

import com.example.warehouse.core.exception.WarehouseException;

public class ValidationException extends WarehouseException {

    private final Set<? extends ConstraintViolation<?>> constraintViolations;

    ValidationException(final Set<? extends ConstraintViolation<?>> constraintViolations) {
        this.constraintViolations = constraintViolations;
    }

    Set<? extends ConstraintViolation<?>> getConstraintViolations() {
        return constraintViolations;
    }
}
