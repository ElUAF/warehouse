package com.example.warehouse.core.validation;

import java.util.Objects;

import javax.validation.Validator;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
class ValidationServiceImpl implements ValidationService {

    private final Validator validator;

    ValidationServiceImpl(final Validator validator) {
        this.validator = validator;
    }

    @Override
    public void validate(@NotNull final Object object) {
        Objects.requireNonNull(object);

        var violations = validator.validate(object);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations);
        }
    }
}
