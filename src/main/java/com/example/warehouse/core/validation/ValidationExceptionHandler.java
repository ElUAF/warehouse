package com.example.warehouse.core.validation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.Builder;
import lombok.Getter;

@ControllerAdvice
class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

    @Builder
    private static class BadRequestResponse {

        @Getter
        private List<ConstraintViolationResponse> violations;
    }

    @Builder
    private static class ConstraintViolationResponse {

        @Getter
        private String propertyPath;

        @Getter
        private String message;
    }

    @ExceptionHandler(value
            = {ValidationException.class})
    protected ResponseEntity<Object> handleConflict(
            ValidationException ex, WebRequest request) {
        return handleExceptionInternal(ex, BadRequestResponse.builder()
                        .violations(ex.getConstraintViolations()
                                .stream()
                                .map(constraintViolation -> ConstraintViolationResponse
                                        .builder()
                                        .propertyPath(constraintViolation.getPropertyPath().toString())
                                        .message(constraintViolation.getMessage())
                                        .build()
                                ).collect(Collectors.toList())
                        )
                        .build(),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
