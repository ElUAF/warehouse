package com.example.warehouse.core.validation;

import org.jetbrains.annotations.NotNull;

public interface ValidationService {

    /**
     * @param object
     *         any object
     * @throws ValidationException
     */
    void validate(@NotNull Object object);
}
