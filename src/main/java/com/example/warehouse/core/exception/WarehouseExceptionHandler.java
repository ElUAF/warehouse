package com.example.warehouse.core.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.Builder;
import lombok.Getter;

@ControllerAdvice
class WarehouseExceptionHandler extends ResponseEntityExceptionHandler {

    @Builder
    private static class EntityNotFoundResponse {

        @Getter
        private String entity;

        @Getter
        private Object id;
    }

    @ExceptionHandler(value
            = {AbstractEntityNotFoundException.class})
    protected ResponseEntity<Object> handleConflict(
            AbstractEntityNotFoundException ex, WebRequest request) {
        return handleExceptionInternal(
                ex,
                EntityNotFoundResponse
                        .builder()
                        .entity(ex.getEntityClass().getSimpleName())
                        .id(ex.getId())
                        .build(),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND, request
        );
    }
}
