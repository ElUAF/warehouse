package com.example.warehouse.core.exception;

public abstract class AbstractEntityNotFoundException extends WarehouseException {

    private final Class entityClass;
    private final Long id;

    public AbstractEntityNotFoundException(final Class entityClass, final Long id) {
        super("entity " + entityClass.getSimpleName() + " with id [" + id + "] is not found");

        this.entityClass = entityClass;
        this.id = id;
    }

    public Class getEntityClass() {
        return entityClass;
    }

    public Long getId() {
        return id;
    }
}
