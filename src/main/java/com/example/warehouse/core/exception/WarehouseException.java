package com.example.warehouse.core.exception;

public class WarehouseException extends RuntimeException {

    public WarehouseException() {
    }

    public WarehouseException(final String message) {
        super(message);
    }

    public WarehouseException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public WarehouseException(final Throwable cause) {
        super(cause);
    }

    public WarehouseException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
