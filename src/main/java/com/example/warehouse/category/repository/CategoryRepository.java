package com.example.warehouse.category.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.util.Streamable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import com.example.warehouse.category.entity.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    @Nullable
    Category findCategoryByName(String name);

    @NotNull
    Streamable<Category> findCategoriesByPathStartsWith(String path);
}
