package com.example.warehouse.category.service;

import java.util.Comparator;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.warehouse.category.api.events.CategoryPreDeletedEvent;
import com.example.warehouse.category.api.exception.CategoryNotFoundException;
import com.example.warehouse.category.dto.CreateCategoryRequest;
import com.example.warehouse.category.entity.Categories;
import com.example.warehouse.category.entity.Category;
import com.example.warehouse.category.repository.CategoryRepository;
import com.example.warehouse.category.rest.CategoryWithChildrenDto;
import com.example.warehouse.core.validation.ValidationService;

@Transactional(readOnly = true)
@Service
class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ValidationService validationService;
    private final ApplicationEventPublisher applicationEventPublisher;

    CategoryServiceImpl(CategoryRepository categoryRepository,
                        ValidationService validationService,
                        ApplicationEventPublisher applicationEventPublisher) {
        this.categoryRepository = categoryRepository;
        this.validationService = validationService;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public CategoryWithChildrenDto findCategoryTreeById(final long parentId) {
        var categories = findAllSubCategoriesById(parentId);

        return toCategoryTree(parentId, categories);
    }

    @Transactional
    @Override
    public void deleteCategory(final long categoryId) {
        var categories = findAllSubCategoriesById(categoryId);

        categories.stream()
                .sorted(Comparator.comparing(Category::getId).reversed())
                .forEach(category -> {
                    applicationEventPublisher.publishEvent(new CategoryPreDeletedEvent(category));
                    categoryRepository.delete(category);
                });
    }

    @NotNull
    @Override
    public Category findExistingCategory(final long categoryId) {
        var category = categoryRepository.findById(categoryId);

        if (category.isEmpty()) {
            throw new CategoryNotFoundException(categoryId);
        }

        return category.get();
    }

    @Transactional
    @Override
    @NotNull
    public Category createCategory(@NotNull final CreateCategoryRequest request) {
        validationService.validate(request);

        var category = Category.builder()
                .name(request.getName())
                .build();

        return categoryRepository.save(category);
    }

    @Override
    @NotNull
    public Category createSubCategory(final long parentId,
                                      @NotNull final CreateCategoryRequest request) {
        validationService.validate(request);

        var parent = findExistingCategory(parentId);

        var category = Category.builder()
                .name(request.getName())
                .parent(parent)
                .build();

        return categoryRepository.save(category);
    }

    private CategoryWithChildrenDto toCategoryTree(long parentId, Categories categories) {
        return categories.findById(parentId)
                .map(rootCategory -> getCategoryWithChildrenDto(rootCategory, categories))
                .orElseThrow(() -> new IllegalStateException(
                        "categories don't contain selected parent: " + parentId)
                );
    }

    private CategoryWithChildrenDto getCategoryWithChildrenDto(Category category, Categories categories) {
        return CategoryWithChildrenDto.builder()
                .category(category)
                .subCategories(category
                        .findDirectSubCategories(categories)
                        .map(subCategory -> getCategoryWithChildrenDto(subCategory, categories))
                        .toList())
                .build();
    }

    /**
     * Finds deeply all subcategories for given category ID
     *
     * @param id
     *         the ID of category for which we would like to find all sub categories
     * @return the category itself and all subcategories so that never returns empty
     * @throws CategoryNotFoundException
     *         if category is not found by given ID
     */
    @NotNull
    private Categories findAllSubCategoriesById(final long id) {
        var category = findExistingCategory(id);
        var categories = categoryRepository.findCategoriesByPathStartsWith(category.getPath());
        return new Categories(categories);
    }
}
