package com.example.warehouse.category.service;

import org.jetbrains.annotations.NotNull;

import com.example.warehouse.category.api.events.CategoryPreDeletedEvent;
import com.example.warehouse.category.api.exception.CategoryNotFoundException;
import com.example.warehouse.category.dto.CreateCategoryRequest;
import com.example.warehouse.category.entity.Category;
import com.example.warehouse.category.rest.CategoryWithChildrenDto;
import com.example.warehouse.core.validation.ValidationException;

public interface CategoryService {

    /**
     * Finds deeply all subcategories for given category ID
     *
     * @param parentId
     *         the ID of category for which we would like to find all sub categories
     * @return the requested category and subcategories in tree structure
     * @throws CategoryNotFoundException
     *         if category is not found by given ID
     */
    CategoryWithChildrenDto findCategoryTreeById(long parentId);

    /**
     * Creates new category.
     *
     * @param request
     *         create category request which contain all necessary information required to create new category
     * @return created category with newly generated ID
     * @throws CategoryNotFoundException
     *         if category is not found by parentId
     * @throws ValidationException
     *         if given request is not valid
     */
    @NotNull
    Category createCategory(@NotNull CreateCategoryRequest request);

    /**
     * Creates new category as subcategory of given category ID.
     *
     * @param parentId
     *         parentId - id of category, which is used as parent for newly created category
     * @param request
     *         create category request which contain all necessary information required to create new category
     * @return created category with newly generated ID
     * @throws CategoryNotFoundException
     *         if category is not found by parentId
     * @throws ValidationException
     *         if given request is not valid
     */
    @NotNull
    Category createSubCategory(long parentId,
                               @NotNull CreateCategoryRequest request);

    /**
     * Delete given category and all sub categories.
     * CategoryPreDeletedEvent event is published for each category before that category is deleted.
     *
     * @throws CategoryNotFoundException
     *         if category is not found by given ID
     * @see CategoryPreDeletedEvent
     */
    void deleteCategory(long categoryId);

    /**
     * Finds existing category in DB or throw CategoryNotFoundException exception
     *
     * @return existing category from DB
     * @throws CategoryNotFoundException
     *         if category is not found by given ID
     */
    @NotNull
    Category findExistingCategory(final long categoryId);
}
