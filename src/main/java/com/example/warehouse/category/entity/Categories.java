package com.example.warehouse.category.entity;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.util.Streamable;

/**
 * Wrapper for collection of category entities
 */
public class Categories {

    private final Streamable<Category> categories;
    private Map<Long, List<Category>> directSubCategoriesCache;

    public Categories(@NotNull final Streamable<Category> categories) {
        this.categories = Objects.requireNonNull(categories, "categories cannot be null");
    }

    @NotNull
    public Optional<Category> findById(long categoryId) {
        return categories.get().filter(category -> category.getId() == categoryId).findFirst();
    }

    @NotNull
    public <T> Streamable<T> map(final Function<Category, T> mapper) {
        Objects.requireNonNull(mapper, "mapper cannot be null");

        return categories.map(mapper);
    }

    public Stream<Category> stream() {
        return categories.get();
    }

    @NotNull
    private Map<Long, List<Category>> getDirectSubCategories() {
        if (directSubCategoriesCache == null) {
            directSubCategoriesCache = categories.get()
                    .filter(category -> category.getParent() != null)
                    .collect(Collectors.groupingBy(Category::getParent, Collectors.toList()));
        }

        return directSubCategoriesCache;
    }

    @NotNull
    Categories findChildren(@NotNull final Category category) {
        Objects.requireNonNull(category, "category cannot be null");
        return new Categories(Streamable
                .of(getDirectSubCategories()
                        .getOrDefault(category.getId(), List.of())
                ));
    }
}
