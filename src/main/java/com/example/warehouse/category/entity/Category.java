package com.example.warehouse.category.entity;

import java.util.UUID;

import org.springframework.data.annotation.Id;

import com.example.warehouse.category.api.CategoryConstants;
import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.category.validation.CategoryName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Category implements CategoryRef {

    @Id
    @Getter
    private Long id;

    @Getter
    @CategoryName
    private String name;

    @Getter
    @JsonIgnore
    private Long parent;

    @Getter
    @JsonIgnore
    private String path;

    @Builder
    public Category(Long id, String name, Category parent) {
        this.id = id;
        this.name = name;
        this.parent = parent != null ? parent.id : null;
        this.path = parent != null ? parent.path + CategoryConstants.CATEGORY_PATH_SEPARATOR + UUID.randomUUID() : UUID.randomUUID().toString();
    }

    public Categories findDirectSubCategories(Categories elements) {
        return elements.findChildren(this);
    }
}
