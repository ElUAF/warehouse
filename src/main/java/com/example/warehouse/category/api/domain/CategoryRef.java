package com.example.warehouse.category.api.domain;

public interface CategoryRef {

    Long getId();
}
