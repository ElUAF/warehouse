package com.example.warehouse.category.api.events;

import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.category.entity.Category;
import com.example.warehouse.category.service.CategoryService;
import com.example.warehouse.core.events.AbstractEntityPreDeletedEvent;

/**
 * Indicates that category is going to be deleted.
 *
 * @see CategoryService#deleteCategory(long)
 */
public class CategoryPreDeletedEvent extends AbstractEntityPreDeletedEvent {

    private final CategoryRef categoryRef;

    public CategoryPreDeletedEvent(CategoryRef categoryRef) {
        super(Category.class, categoryRef.getId());

        this.categoryRef = categoryRef;
    }

    public CategoryRef getCategoryRef() {
        return categoryRef;
    }
}
