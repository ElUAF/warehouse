package com.example.warehouse.category.api.service;

import org.jetbrains.annotations.NotNull;

import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.category.api.exception.CategoryNotFoundException;

/**
 * Public service, which can be used by other aggregates.
 */
public interface CategoryPublicService {

    /**
     * Finds existing category from DB by given ID.
     *
     * @return category ref
     * @throws CategoryNotFoundException
     *         if cateogry is not found by given category ID
     */
    @NotNull
    CategoryRef findExistingCategory(long categoryId);
}
