package com.example.warehouse.category.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import com.example.warehouse.category.api.domain.CategoryRef;
import com.example.warehouse.category.service.CategoryService;

@Service
class CategoryPublicServiceImpl implements CategoryPublicService {

    private final CategoryService categoryService;

    public CategoryPublicServiceImpl(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @NotNull
    @Override
    public CategoryRef findExistingCategory(final long categoryId) {
        return categoryService.findExistingCategory(categoryId);
    }
}
