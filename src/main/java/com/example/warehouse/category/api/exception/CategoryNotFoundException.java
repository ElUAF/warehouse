package com.example.warehouse.category.api.exception;

import com.example.warehouse.category.entity.Category;
import com.example.warehouse.core.exception.AbstractEntityNotFoundException;

public class CategoryNotFoundException extends AbstractEntityNotFoundException {

    public CategoryNotFoundException(final Long id) {
        super(Category.class, id);
    }
}
