package com.example.warehouse.category.api;

public class CategoryConstants {

    public static final String CATEGORY_PATH_SEPARATOR = "#";

    private CategoryConstants() {
        // only constants
    }
}
