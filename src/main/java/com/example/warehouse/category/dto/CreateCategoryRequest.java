package com.example.warehouse.category.dto;

import com.example.warehouse.category.validation.CategoryName;
import com.example.warehouse.category.validation.UniqueCategoryName;
import com.fasterxml.jackson.annotation.JsonCreator;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class CreateCategoryRequest {

    @Getter
    @Setter
    @CategoryName
    @UniqueCategoryName
    private String name;

    @Builder
    @JsonCreator
    public CreateCategoryRequest(String name) {
        this.name = name;
    }

}
