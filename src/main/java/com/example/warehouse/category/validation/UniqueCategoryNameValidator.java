package com.example.warehouse.category.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.warehouse.category.repository.CategoryRepository;

public class UniqueCategoryNameValidator implements ConstraintValidator<UniqueCategoryName, String> {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public void initialize(final UniqueCategoryName constraintAnnotation) {
        // we don't need any settings at the moment
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        if (value == null || value.isBlank()) {
            return true;
        }

        return categoryRepository.findCategoryByName(value) == null;
    }
}
