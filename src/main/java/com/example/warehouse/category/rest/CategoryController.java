package com.example.warehouse.category.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.warehouse.category.dto.CreateCategoryRequest;
import com.example.warehouse.category.entity.Category;
import com.example.warehouse.category.service.CategoryService;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;

    CategoryController(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping
    public @NotNull
    Category createCategory(@RequestBody CreateCategoryRequest createCategoryRequest) {
        return categoryService.createCategory(createCategoryRequest);
    }

    @PostMapping("/{id}/create-sub-category")
    @NotNull
    public Category createSubCategory(@PathVariable("id") long parentId,
                                      @RequestBody @NotNull CreateCategoryRequest createCategoryRequest) {
        return categoryService.createSubCategory(parentId, createCategoryRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCategoryById(@PathVariable("id") long id) {
        categoryService.deleteCategory(id);
    }

    @GetMapping("/{id}/category-tree")
    @NotNull
    public CategoryWithChildrenDto getCategoryTreeById(@PathVariable("id") long parentId) {
        return categoryService.findCategoryTreeById(parentId);
    }
}
