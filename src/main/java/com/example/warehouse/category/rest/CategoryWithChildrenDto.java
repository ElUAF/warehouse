package com.example.warehouse.category.rest;

import java.util.List;
import java.util.Objects;

import com.example.warehouse.category.entity.Category;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class CategoryWithChildrenDto {

    @Getter
    @JsonUnwrapped
    private Category category;

    @Getter
    private List<CategoryWithChildrenDto> subCategories;

    @Builder
    CategoryWithChildrenDto(Category category, List<CategoryWithChildrenDto> subCategories) {
        this.category = Objects.requireNonNull(category, "category cannot be null");
        this.subCategories = subCategories != null ? subCategories : List.of();
    }

}
