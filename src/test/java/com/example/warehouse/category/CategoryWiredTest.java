package com.example.warehouse.category;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.example.warehouse.AbstractWiredTest;
import com.example.warehouse.category.api.exception.CategoryNotFoundException;
import com.example.warehouse.category.rest.CategoryController;
import com.example.warehouse.category.rest.CategoryWithChildrenDto;
import com.example.warehouse.core.validation.ValidationException;

@SpringBootTest
@Transactional
class CategoryWiredTest extends AbstractWiredTest {

    @Autowired
    private CategoryController categoryController;

    @Test
    void createCategory_validArguments_success() {
        var request = defaultCreateCategoryRequest().build();

        var actual = categoryController.createCategory(request);

        Assertions.assertEquals(request.getName(), actual.getName());
    }

    @Test
    void createSubCategory_validArguments_success() {
        var root = givenCategory();
        var request = defaultCreateCategoryRequest().build();

        var actual = categoryController.createSubCategory(root.getId(), request);

        Assertions.assertEquals(request.getName(), actual.getName());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    void createCategory_invalidNames_success(String invalidName) {
        Assertions.assertThrows(ValidationException.class, () -> categoryController
                .createCategory(
                        defaultCreateCategoryRequest()
                                .name(invalidName)
                                .build()
                )
        );
    }

    @Test
    void createCategory_sameName_throwsValidationException() {
        Assertions.assertThrows(ValidationException.class, () -> {
            categoryController.createCategory(defaultCreateCategoryRequest().build());
            categoryController.createCategory(defaultCreateCategoryRequest().build());
        });
    }

    @Test
    void getCategoryTreeById_categoryNotFound_throwsCategoryNotFoundException() {
        Assertions.assertThrows(CategoryNotFoundException.class, () -> categoryController.getCategoryTreeById(1));
    }

    @Test
    void getCategoryTreeById_categoryWithoutSubCategories_returnsOnlyThatCategory() {
        var category = givenCategory();

        var expected = CategoryWithChildrenDto.builder()
                .category(category)
                .build();

        var actual = categoryController.getCategoryTreeById(category.getId());

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getCategoryTreeById_categoryWithoutSubCategoriesAndAnotherCategory_returnsOnlyThatCategory() {
        givenCategory();
        var category = givenCategory();

        var expected = CategoryWithChildrenDto.builder()
                .category(category)
                .build();

        var actual = categoryController.getCategoryTreeById(category.getId());

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getCategoryTreeById_categoryWithSubCategories_returnThatCategoryAndAllSubCategories() {
        var category = givenCategory();
        var subCategory1 = givenSubCategory(category);
        var subCategory2 = givenSubCategory(category);
        var subCategory3 = givenSubCategory(subCategory2);

        var expected = CategoryWithChildrenDto.builder()
                .category(category)
                .subCategories(List.of(
                        CategoryWithChildrenDto.builder()
                                .category(subCategory1)
                                .build(),
                        CategoryWithChildrenDto.builder()
                                .category(subCategory2)
                                .subCategories(List.of(
                                        CategoryWithChildrenDto.builder()
                                                .category(subCategory3)
                                                .build()
                                ))
                                .build()
                )).build();

        var actual = categoryController.getCategoryTreeById(category.getId());

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deleteCategoryById_categoryNotFound_throwsCategoryNotFoundException() {
        Assertions.assertThrows(CategoryNotFoundException.class, () -> categoryController.deleteCategoryById(0));
    }

    @Test
    void deleteCategoryById_existingCategoryAndSubCategoryAndProductIsAssignedToAllCategories_categoryAndSubCategoryAreDeleted() {
        var category = givenCategory();
        var subCategory = givenSubCategory(category);
        var subCategory2 = givenSubCategory(subCategory);
        var product1 = givenProductInCategory(category);
        var product2 = givenProductInCategory(subCategory);
        var product3 = givenProductInCategory(subCategory2);

        categoryController.deleteCategoryById(category.getId());

        Assertions.assertAll(
                () -> thenCategoryNotExists(category),
                () -> thenCategoryNotExists(subCategory),
                () -> thenCategoryNotExists(subCategory2),
                () -> thenProductNotInCategory(product1, category),
                () -> thenProductNotInCategory(product2, subCategory),
                () -> thenProductNotInCategory(product3, subCategory2)
        );
    }
}
