package com.example.warehouse;

import java.util.List;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.example.warehouse.category.api.exception.CategoryNotFoundException;
import com.example.warehouse.category.dto.CreateCategoryRequest;
import com.example.warehouse.category.entity.Category;
import com.example.warehouse.category.service.CategoryService;
import com.example.warehouse.product.dto.CreateProductRequest;
import com.example.warehouse.product.entity.Product;
import com.example.warehouse.product.service.ProductService;

@SpringBootTest
@Transactional
public abstract class AbstractWiredTest {

    public static final String DEFAULT_CATEGORY_NAME = "category";

    public static final String DEFAULT_PRODUCT_NAME = "product";
    public static final String DEFAULT_PRODUCT_DESCRIPTION = "description";
    public static final int DEFAULT_PRICE = 10;

    private int categoryCounter;
    private int productCounter;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @NotNull
    protected Category givenCategory() {
        return categoryService.createCategory(defaultCreateCategoryRequest()
                .name(DEFAULT_CATEGORY_NAME + categoryCounter++)
                .build());
    }

    @NotNull
    protected Category givenSubCategory(@NotNull Category parent) {
        return categoryService.createSubCategory(parent.getId(), defaultCreateCategoryRequest()
                .name(DEFAULT_CATEGORY_NAME + categoryCounter++)
                .build());
    }

    @NotNull
    protected CreateCategoryRequest.CreateCategoryRequestBuilder defaultCreateCategoryRequest() {
        return CreateCategoryRequest.builder()
                .name(DEFAULT_CATEGORY_NAME);
    }

    @NotNull
    protected Product givenProduct() {
        return productService.createProduct(defaultCreateProductRequest()
                .name(DEFAULT_PRODUCT_NAME + productCounter++)
                .build());
    }

    @NotNull
    protected Product givenProductInCategory(@NotNull Category category) {
        var product = productService.createProduct(defaultCreateProductRequest()
                .name(DEFAULT_PRODUCT_NAME + productCounter++)
                .build());

        productService.addProductToCategory(product.getId(), category.getId());

        return product;
    }

    @NotNull
    protected CreateProductRequest.CreateProductRequestBuilder defaultCreateProductRequest() {
        return CreateProductRequest.builder()
                .name(DEFAULT_PRODUCT_NAME)
                .description(DEFAULT_PRODUCT_DESCRIPTION)
                .price(DEFAULT_PRICE);
    }

    protected void thenProductsInCategory(@NotNull List<Product> expected, @NotNull Category category) {
        var actual = productService.findProductsByCategoryIdRecursively(category.getId());

        thenExpectedProducts(expected, actual);
    }

    protected void thenProductNotInCategory(@NotNull Product product, @NotNull Category category) {
        var actualProduct = productService.findProductById(product.getId());

        Assertions.assertFalse(actualProduct.hasCategory(category), "Product shouldn't have category " + category.getId() + " product: " + actualProduct);
    }

    protected void thenExpectedProduct(@NotNull Product expected, @NotNull Product actual) {
        Assertions.assertEquals(expected.getId(), actual.getId());
    }

    protected void thenExpectedProducts(@NotNull List<Product> expected, @NotNull List<Product> actual) {
        Assertions.assertEquals(
                expected.stream().map(Product::getId).collect(Collectors.toList()),
                actual.stream().map(Product::getId).collect(Collectors.toList()),
                "products should match"
        );
    }

    protected void thenCategoryNotExists(@NotNull Category category) {
        Assertions.assertThrows(
                CategoryNotFoundException.class,
                () -> categoryService.findExistingCategory(category.getId()),
                "category shouldn't exists"
        );
    }
}
