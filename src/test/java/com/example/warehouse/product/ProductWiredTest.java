package com.example.warehouse.product;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.warehouse.AbstractWiredTest;
import com.example.warehouse.category.api.exception.CategoryNotFoundException;
import com.example.warehouse.core.validation.ValidationException;
import com.example.warehouse.product.rest.ProductController;

class ProductWiredTest extends AbstractWiredTest {

    @Autowired
    private ProductController productController;

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    void createProduct_invalidName_throwsValidationException(String invalidName) {
        Assertions.assertThrows(ValidationException.class, () -> productController.createProduct(
                defaultCreateProductRequest().name(invalidName).build())
        );
    }

    @Test
    void createProduct_valid_productCanBeLoaded() {
        var product = givenProduct();

        var loadedProduct = productController.findProductById(product.getId());

        thenExpectedProduct(product, loadedProduct);
    }

    @Test
    void findProductsByCategoryId_categoryNotExists_throwsCategoryNotFoundException() {
        Assertions.assertThrows(CategoryNotFoundException.class, () -> productController.findProductsByCategoryId(0));
    }

    @Test
    void findProductsByCategoryId_oneProductWithRequestedCategory_returnsOneProducts() {
        var category = givenCategory();
        var product = givenProductInCategory(category);

        var loadedProducts = productController.findProductsByCategoryId(category.getId());

        thenExpectedProducts(List.of(product), loadedProducts);
    }

    @Test
    void oneProductWithCategoryAndRequestedParentCategory_returnsOneProducts() {
        var root = givenCategory();
        var category = givenSubCategory(root);
        var product = givenProductInCategory(category);

        thenProductsInCategory(List.of(product), root);
    }

    @Test
    void addProductToCategory_oneProductWithSameCategory_oneProductIsFoundOnThatCategory() {
        var category = givenCategory();
        var product = givenProductInCategory(category);

        productController.addProductToCategory(product.getId(), category.getId());

        thenProductsInCategory(List.of(product), category);
    }

    @Test
    void removeProductFromCategory_productOriginallyHasSameCategory_noProductIsFoundOnThatCategory() {
        var category = givenCategory();
        var product = givenProductInCategory(category);

        productController.removeProductFromCategory(product.getId(), category.getId());

        thenProductsInCategory(List.of(), category);
    }

}
